import { Injectable } from '@angular/core';
import { HomeComponent } from './home.component';
import { NzMessageService } from 'ng-zorro-antd/message';

import * as _ from 'lodash';
import * as State from './home.state';
import * as utils from '../../../common/utils';
import { ObserveSubjectService } from 'src/common/service';

@Injectable()
export class HomeBizService {
  private component: HomeComponent;
  constructor(
    private nzMessageSvc: NzMessageService,
    private observeSubjectSvc: ObserveSubjectService) {

  }

  setComponent(component: HomeComponent) {
    this.component = component;
  }

  initHome() {
    const { UIModle, UICtrl } = this.component;
    const localConfig = utils.getStorageValue('ui_config', 'local', true);
    UICtrl.config = { ...UICtrl.config, ...localConfig };
    let i = 1;
    while (i < 35) {
      UIModle.appGroupList[0].push({
        progId: `${i + 1}`.padStart(6, '0'),
        order: 0,
        name: `${i + 1}`.padStart(6, '0'),
        imgUrl: '../../../assets/app-imgs/tub.svg',
        urlPath: '/admin/dome-page',
        backgroundColor: `#${utils.getABNumber(0, 9)}${utils.getABNumber(0, 9)}${utils.getABNumber(0, 9)}${utils.getABNumber(0, 9)}${utils.getABNumber(0, 9)}${utils.getABNumber(0, 9)}`
      });
      UIModle.appGroupList[1].push({
        progId: `${i + 1}`.padStart(6, '0'),
        order: 0,
        name: `${i + 1}`.padStart(6, '0'),
        imgUrl: '../../../assets/app-imgs/tub.svg',
        urlPath: '/admin/dome-page',
        backgroundColor: `#${utils.getABNumber(0, 9)}${utils.getABNumber(0, 9)}${utils.getABNumber(0, 9)}${utils.getABNumber(0, 9)}${utils.getABNumber(0, 9)}${utils.getABNumber(0, 9)}`
      });
      i++;
    }
    this.resetStyle();
    this.observeSubjectSvc.actionSubject.subscribe((value) => {
      if (value.action === 'edit-home') {
        this.resetInitDrawer();
      }
      if (value.action === 'home-setting') {
        this.resetInitEditData();
      }
    });
  }

  resetInitEditData() {
    const { UIModle, UICtrl } = this.component;
    UIModle.pageIndex = 0;
    UIModle.appGroupEditStateList = _.cloneDeep(UIModle.appGroupList);
    UICtrl.isEditState = true;
  }

  resetInitDrawer() {
    const { UIModle, UICtrl } = this.component;
    UIModle.setConfig = _.cloneDeep(UICtrl.config);
    UICtrl.visibleDrawer = true;
  }

  resetStyle() {
    const { UIStyle, UICtrl: { config } } = this.component;
    const ulWidth = document.documentElement.clientWidth - 120;
    const liWidth = config.appSize + 2 * config.appBaseMagrin;
    const rowItems = Math.floor(ulWidth / liWidth);
    const residueW = (ulWidth) % liWidth;
    const newMargin = config.appBaseMagrin + residueW / rowItems / 2;
    UIStyle.appstyle = {
      ...UIStyle.appstyle,
      width: `${config.appSize}px`,
      height: `${config.appSize}px`,
      margin: `${newMargin}px`
    };
  }


  addApp() {
    const { UIModle: { selectedValue, appGroupEditStateList, pageIndex }, UICtrl } = this.component;
    const progId = selectedValue.progId;
    if (!progId) {
      this.nzMessageSvc.warning('You no selected anything !');
      return;
    }
    // 判断 是否重复
    if (appGroupEditStateList[pageIndex].some(item => item.progId === progId)) {
      this.nzMessageSvc.warning('This App is already existed !');
      return;
    }
    selectedValue.order = appGroupEditStateList[pageIndex].length;
    this.component.UIModle.selectedValue = new State.APPItem();
    appGroupEditStateList[pageIndex].push(_.cloneDeep(selectedValue));
    UICtrl.isVisible = false;
  }

  onDrop(e: any, targetArray: Array<State.APPItem>) {
    const { UICtrl } = this.component;
    const { dragFromIndex: starIndex, dropIndex: endIndex, dragData: { item } } = e;
    if (UICtrl.config.allowEmpty) {
      targetArray.splice(endIndex, 0, targetArray.splice(starIndex, 1)[0]);
    } else {
      const splice = !_.some(targetArray[endIndex]);
      const emptyeItem = new State.APPItem();
      targetArray.splice(endIndex, splice ? 1 : 0, targetArray.splice(starIndex, 1, emptyeItem)[0]);
      this.removeLastEmptyItems(targetArray);
    }
  }


  // rmove last empty element
  removeLastEmptyItems(targetArray: Array<State.APPItem>) {
    if (!targetArray[targetArray.length - 1].progId) {
      targetArray.pop();
      this.removeLastEmptyItems(targetArray);
    }
  }


  editSave() {
    const { UIModle, UICtrl } = this.component;
    UIModle.appGroupList = _.cloneDeep(UIModle.appGroupEditStateList);
    UICtrl.isEditState = false;
  }

  setConfigApply() {
    const { UIModle, UICtrl } = this.component;
    UICtrl.config = { ...UICtrl.config, ...UIModle.setConfig };
    utils.setStorageValue('ui_config', UICtrl.config, 'local');
    this.resetStyle();
    UICtrl.visibleDrawer = false;
  }
}
