import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home.component';
import { NzCarouselModule } from 'ng-zorro-antd/carousel';
import { NzGridModule } from 'ng-zorro-antd/grid';
import { NzSliderModule } from 'ng-zorro-antd/slider';
import { FormsModule } from '@angular/forms';
import { DevUIModule } from 'ng-devui';
import { NzIconModule, NzMessageContainerComponent } from 'ng-zorro-antd';
import { NzModalModule } from 'ng-zorro-antd/modal';
import { NzSelectModule } from 'ng-zorro-antd/select';
import { NzMessageModule } from 'ng-zorro-antd/message';
import { RouterModule } from '@angular/router';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzDrawerModule } from 'ng-zorro-antd/drawer';
import { NzSwitchModule } from 'ng-zorro-antd/switch';
import { NzInputNumberModule } from 'ng-zorro-antd/input-number';
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RouterModule,
    NzCarouselModule,
    NzGridModule,
    NzSliderModule,
    DevUIModule,
    NzIconModule,
    NzModalModule,
    NzSelectModule,
    NzMessageModule,
    NzButtonModule,
    NzDrawerModule,
    NzSwitchModule,
    NzInputNumberModule
  ],
  exports: [HomeComponent],
  declarations: [HomeComponent]
})
export class HomeModule { }
