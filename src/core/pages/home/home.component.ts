import { AfterViewInit, Component, OnInit } from '@angular/core';
import { HomeBizService } from './home.biz.service';

import * as State from './home.state';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  providers: [HomeBizService]
})
export class HomeComponent implements OnInit, AfterViewInit {
  UICtrl = State.UICtrl;
  UIStyle = State.UIStyle;
  UIModle = new State.UIModle();
  constructor(private homeBizSvc: HomeBizService) {
  }

  ngOnInit() {
    this.homeBizSvc.setComponent(this);
    this.homeBizSvc.initHome();
  }

  ngAfterViewInit(): void {
    window.onresize = event => {
      this.homeBizSvc.resetStyle();
    };
  }

  nzBeforeChange(event) {
    this.UIModle.pageIndex = event.to;
  }

  onDrop(e: any, targetArray: any) {
    this.homeBizSvc.onDrop(e, targetArray);
  }

  // 删除
  delete(list: Array<any>, index) {
    list.splice(index, 1);

  }

  openModal() {
    this.UICtrl.isVisible = true;
  }

  handleCancel() {
    this.UICtrl.isVisible = false;
  }

  addApp() {
    this.homeBizSvc.addApp();
  }

  editCancel() {
    this.UICtrl.isEditState = false;
  }

  editSave() {
    this.homeBizSvc.editSave();
  }

  drawerclose() {
    this.UICtrl.visibleDrawer = false;
  }

  configCancel() {
    this.UICtrl.visibleDrawer = false;
  }

  configDefault() {
    this.UIModle.setConfig = new State.Config();
  }

  configApply() {
    this.homeBizSvc.setConfigApply();
  }
}
