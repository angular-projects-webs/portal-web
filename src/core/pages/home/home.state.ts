export const UIStyle = {
  placeholderStyle: {
    backgroundColor: '#F2F6FC',
    border: `2px dashed #E4E7ED`,
    'border-radius': `8px`,
  },

  originPlaceholder: {
    text: '',
    style: {
      backgroundColor: `#F2F6FC`,
      'border-radius': `8px`
    }
  },
  appstyle: {
    margin: '10px',
    width: `150px`,
    height: `150px`,
    'border-radius': `10px`
  },

  // UlStyle: {
  //   height: 'calc(94vh - 24px)'
  // },
  appEditStateStyle: {
  }
};

export class APPItem {
  progId: string;
  order?: number;
  name: string | null;
  imgUrl?: string;
  urlPath: string;
  backgroundColor?: string;
}

export class Config {
  allowEmpty: boolean = false;
  nzAutoPlay: boolean = false;
  nzEnableSwipe: boolean = false;
  nzAutoPlaySpeed: number = 3000;
  nzEffect: 'scrollx' | 'fade' = 'scrollx';
  appSize: number = 150;
  appBaseMagrin: number = 10;
}

export const UICtrl = {
  isEditState: false,
  isVisible: false,
  visibleDrawer: false,
  config: new Config()
};

export class UIModle {
  selectedValue: APPItem;
  pageIndex: number = 0;
  selectedlist: Array<APPItem> = [
    {
      progId: '0040004',
      order: 10,
      name: '8888888',
      imgUrl: '',
      urlPath: '',
      backgroundColor: 'red'
    }
  ];
  appGroupList = [
    [
      {
        progId: '000001',
        order: 0,
        name: '8888888',
        imgUrl: '../../../assets/app-imgs/tub.svg',
        urlPath: '',
        backgroundColor: 'red'
      }
    ],
    [
      {
        progId: '000002',
        order: 0,
        name: '8888888',
        imgUrl: '../../../assets/app-imgs/tub.svg',
        urlPath: '',
        backgroundColor: 'red'
      }
    ],
    []
  ];
  appGroupEditStateList = [];
  setConfig = new Config();
}
