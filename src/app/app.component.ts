import { Component, OnDestroy, OnInit } from '@angular/core';
import { NavigationEnd, Router, RouterEvent } from '@angular/router';
import { filter } from 'rxjs/operators';
import { Planet, SwitchModes } from '@worktile/planet';
import { AuthService, ObserveSubjectService } from 'src/common/service';
import { Apps } from './apps-config';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy {
  visible = false;
  islogin = false;
  currentUrl = '';
  private subscribe;

  get loadingDone() {
    return this.planet.loadingDone;
  }
  constructor(
    private planet: Planet,
    private router: Router,
    private observeSubjectSvc: ObserveSubjectService,
    private authService: AuthService) {
  }

  ngOnInit() {
    if (this.authService.isAuth()) {
      this.navigateHome();
    } else {
      this.authService.removeAuth();
    }
    this.subscribe = this.router.events.pipe(filter(event => event instanceof NavigationEnd))
      .subscribe((event: RouterEvent) => {
        ({url: this.currentUrl  } = event);
        this.islogin = this.authService.isAuth();
      });

    this.planet.setOptions({
      switchMode: SwitchModes.default,
      errorHandler: error => {
        console.error(`Failed to load resource, error:`, error);
      }
    });
    // common Dataa
    this.planet.setPortalAppData({
      foundationSvc: {}
    });

    // 注册子应用
    this.registerApps();

    // start monitor route changes
    // get apps to active by current path
    // load static resources which contains javascript and css
    // bootstrap angular sub app module and show it
    this.planet.start();
  }

  ngOnDestroy(): void {
    this.subscribe.unsubscribe();
    this.observeSubjectSvc.unsubscribeSubject();
  }

  registerApps() {
    this.planet.registerApps(Apps);
  }

  onMenuSwith(event) {
    this.visible = !this.visible;
  }

  navigateHome() {
    if (!/^.+/.test(location.pathname)) {
      this.router.navigate(['/home']);
    }
  }
}
