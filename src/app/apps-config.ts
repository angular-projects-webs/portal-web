import { SwitchModes } from '@worktile/planet';

export const Apps = [
  {
    name: 'admin-web',
    hostParent: '#app-root-portal',
    hostClass: 'thy-layout',
    routerPathPrefix: '/admin',
    selector: 'admin-app-root',
    preload: false,
    manifest: 'admin-web/manifest.json',
    scripts: [
      'admin-web/main.js',
    ],
    styles: [
      'admin-web/styles.css'
    ]
  },
  {
    name: 'ten-web',
    hostParent: '#app-root-portal',
    hostClass: 'thy-layout',
    routerPathPrefix: '/ten',
    selector: 'ten-app-root',
    preload: false,
    manifest: 'ten_proxy/ten-web/manifest.json',
    scripts: [
      'ten_proxy/ten-web/main.js',
    ],
    styles: [
      // 'ten_proxy/ten-web/styles.css', // 请求不到
      'ten_proxy/ten-web/ten-styles.css'
    ]
  },

  {
    name: 'sys_management_web',
    hostParent: '#app-root-portal',
    routerPathPrefix: '/avatar2/module/sys-management',
    selector: 'system-management-root-container',
    preload: false,
    switchMode: SwitchModes.default,
    loadSerial: false,
    manifest: '/sys_management_proxy/sys_management_web/manifest.json',
    scripts: [
      '/sys_management_proxy/sys_management_web/main.js',
    ],
    styles: [
      '/sys_management_proxy/sys_management_web/sys.css'
    ],
  },
];
