import { Component, Input, OnInit } from '@angular/core';
import { ObserveSubjectService } from 'src/common/service';

@Component({
  selector: 'app-menu-component',
  templateUrl: './menu-component.component.html',
  styleUrls: ['./menu-component.component.scss']
})
export class MenuComponentComponent implements OnInit {
  @Input() visible = false;

  openMap: { [name: string]: boolean } = {
    sub1: false,
    sub2: false,
    sub3: false
  };
  constructor(private observeSubjectSvc: ObserveSubjectService) { }

  ngOnInit() {
    this.observeSubjectSvc.actionSubject.subscribe((value) => {
      if (value.action === 'switch-menu') {
        this.visible = !this.visible;
      }
    });
  }

  openHandler(value: string): void {
    for (const key in this.openMap) {
      if (key !== value) {
        this.openMap[key] = false;
      }
    }
  }

  open(): void {
    this.visible = true;
  }

  close(): void {
    this.visible = false;
  }
}
