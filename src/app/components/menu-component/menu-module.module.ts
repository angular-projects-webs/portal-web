import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NzDrawerModule } from 'ng-zorro-antd/drawer';
import { NzMenuModule } from 'ng-zorro-antd/menu';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { NzToolTipModule } from 'ng-zorro-antd/tooltip';
import { MenuComponentComponent } from './menu-component.component';
import { RouterModule } from '@angular/router';
@NgModule({
  imports: [
    CommonModule,
    NzDrawerModule,
    NzMenuModule,
    NzIconModule,
    NzToolTipModule,
    RouterModule

  ],
  exports: [MenuComponentComponent],
  declarations: [MenuComponentComponent]
})
export class MenuModuleModule { }
