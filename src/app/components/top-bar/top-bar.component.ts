import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { AuthService, ObserveSubjectService } from 'src/common/service';


@Component({
  selector: 'app-top-bar',
  templateUrl: './top-bar.component.html',
  styleUrls: ['./top-bar.component.scss']
})
export class TopBarComponent implements OnInit {
  visible: boolean;
  isHome = false;
  selectedLanguage = 'en';

  @Input() set currentUrl(value) {
    this.isHome = /^\/home$/.test(value);
  }

  @Output() menuSwith = new EventEmitter();

  constructor(
    private observeSubjectSvc: ObserveSubjectService,
    private authService: AuthService) { }

  ngOnInit() {
  }

  onclickMenu(event) {
    // this.menuSwith.emit();
    this.observeSubjectSvc.dispatchGeneralAction('switch-menu');
  }

  change(value: boolean): void {
    // console.log(value);
  }

  logOut() {
    this.authService.removeAuth();
  }

  goToHome() {
    this.authService.goToHome();
  }

  openHoneEdit() {
    this.visible = false;
    this.observeSubjectSvc.dispatchGeneralAction('edit-home');
  }

  setPrifle() {
    this.visible = false;
    // this.observeSubjectSvc.dispatchGeneralAction('edit-home');
  }

  onOption() {
    this.isHome ? this.observeSubjectSvc.dispatchGeneralAction('home-setting') :
      this.goToHome();
  }
}
