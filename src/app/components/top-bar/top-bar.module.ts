import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TopBarComponent } from './top-bar.component';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { FormsModule } from '@angular/forms';
import { NzBadgeModule } from 'ng-zorro-antd/badge';
import { NzPopoverModule } from 'ng-zorro-antd/popover';
import { NzButtonModule } from 'ng-zorro-antd';
import { NzToolTipModule } from 'ng-zorro-antd/tooltip';
import { NzSelectModule } from 'ng-zorro-antd/select';
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    NzIconModule,
    NzBadgeModule,
    NzPopoverModule,
    NzButtonModule,
    NzToolTipModule,
    NzSelectModule
  ],
  exports: [TopBarComponent],
  declarations: [TopBarComponent]
})
export class TopBarModule { }
