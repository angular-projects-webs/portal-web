import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NgxPlanetModule } from '@worktile/planet';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MenuModuleModule } from './components/menu-component/menu-module.module';
import { TopBarModule } from './components/top-bar/top-bar.module';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgxPlanetModule,
    BrowserAnimationsModule,
    MenuModuleModule,
    TopBarModule
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
