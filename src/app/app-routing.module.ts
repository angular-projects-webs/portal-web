import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EmptyComponent } from '@worktile/planet';
import { HomeComponent } from 'src/core/pages/home/home.component';
import { HomeModule } from 'src/core/pages/home/home.module';
import { LoginComponent } from 'src/core/pages/login/login.component';
import { LoginModule } from 'src/core/pages/login/login.module';
import { AuthGuard } from '../core/auth-guard';

const routes: Routes = [
  {
    path: 'login',
    component: LoginComponent,
    // children: [
    //   {
    //     path: '**',
    //     component: EmptyComponent
    //   }
    // ]
  },
  {
    path: 'home',
    component: HomeComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'admin',
    component: EmptyComponent,
    children: [
      {
        path: '**',
        component: EmptyComponent
      }
    ],
    canActivate: [AuthGuard]
  },
  {
    path: 'ten',
    component: EmptyComponent,
    children: [
      {
        path: '**',
        component: EmptyComponent
      }
    ],
    canActivate: [AuthGuard]
  },
  {
    path: 'avatar2/module/sys-management',
    component: EmptyComponent,
    data: {
      planet: 'Y'
    },
    children: [
      {
        path: '**',
        component: EmptyComponent,
        data: {
          planet: 'Y'
        },
        // canActivate: [AuthGuard]
      }
    ],
    // canActivate: [AuthGuard]
  },
];

@NgModule({
  imports: [
    LoginModule,
    HomeModule,
    RouterModule.forRoot(routes)
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
