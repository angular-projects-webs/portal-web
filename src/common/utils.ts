import { isString } from 'util';

export const setStorageValue = (key: string, value, type: 'local' | 'session' = 'session') => {
  let strValue = '';
  if (isString(value)) {
    strValue = value;
  } else {
    strValue = JSON.stringify(value);
  }
  if (type === 'local') {
    window.localStorage.setItem(key, strValue);
    return;
  }
  window.sessionStorage.setItem(key, strValue);
};

export const getStorageValue = (key: string, type: 'local' | 'session' = 'session', isSwitchObject: boolean = false) => {
  let value = null;
  if (!key) {
    return value;
  }
  if (type === 'local') {
    value = window.localStorage.getItem(key);
  } else {
    value = window.sessionStorage.getItem(key);
  }
  if (isSwitchObject && value) {
    return JSON.parse(value);
  }
  return value;
};

export const getABNumber = (a: number, b: number) => {
  return parseInt(String(Math.random() * (b - a + 1))) + a;
}
