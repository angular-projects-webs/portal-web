import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import * as unitl from '../utils';
@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private router: Router) { }

  getToken() {
    return unitl.getStorageValue('token');
  }

  isAuth(): boolean {
    if (unitl.getStorageValue('token')) {
      return true;
    }
    return false;
  }

  removeAuth() {
    window.sessionStorage.clear();
    this.router.navigate(['/login']);
  }

  goToHome() {
    this.router.navigate(['/home']);
  }
}
