const PROXY_CONFIG = {};

PROXY_CONFIG["/admin-web"] = {
  target: "http://localhost:3001",
  secure: false,
  changeOrigin: true,
  pathRewrite: { // 对请求路径进行重定向以匹配到正确的请求地址
    "^/admin-web": "/admin-web",
  },
};

PROXY_CONFIG["/ten_proxy"] = {
  target: "http://localhost:3002",
  secure: false,
  changeOrigin: true,
  pathRewrite: { // 对请求路径进行重定向以匹配到正确的请求地址
    "^/ten_proxy": "",
  },
};

PROXY_CONFIG["/sys_management_proxy"] = {
  target: "http://localhost:3003",
  secure: false,
  changeOrigin: true,
  pathRewrite: {
    "^/sys_management_proxy": "",
  },
};

module.exports = PROXY_CONFIG;
